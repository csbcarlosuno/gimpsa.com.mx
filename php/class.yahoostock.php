<?php
/**
 * Class to fetch stock data from Yahoo! Finance
 *
 */
 
class YahooStock {
     
    /**
     * Array of stock code
     */
    private $stocks = array();
     
    /**
     * Parameters string to be fetched  
     */
    private $format;
 
    /**
     * Populate stock array with stock code
     *
     * @param string $stock Stock code of company   
     * @return void
     */
    public function addStock($stock)
    {
        $this->stocks[] = $stock;
    }
     
    /**
     * Populate parameters/format to be fetched
     *
     * @param string $param Parameters/Format to be fetched
     * @return void
     */
    public function addFormat($format)
    {
        $this->format = $format;
    }
 
    /**
     * Get Stock Data
     *
     * @return array
     */
    public function getQuotes()
    {       
        $strStock = null;
        foreach($this->stocks as $stock)
        {
            $strStock .= strtolower(str_replace(".","",$stock));
        }
        $date   = date('mdY');
        $filename = "stocks/" . $date . "_" . $strStock . ".json";
        $result = array();     
        $format = $this->format;
         
        if(file_exists($filename))
        {
            $json = file_get_contents($filename);
            return json_decode($json);
        }

        foreach ($this->stocks as $stock)
        {           
            /**
             * fetch data from Yahoo!
             * s = stock code
             * f = format
             * e = filetype
             */
            $s = file_get_contents("http://finance.yahoo.com/d/quotes.csv?s=$stock&f=$format&e=.csv");
             
            /**
             * convert the comma separated data into array
             */
            $data = explode( ',', $s);
             
            /**
             * populate result array with stock code as key
             */
            $result[$stock] = $data;
        }
        $file = fopen($filename, "w+");
        $json = json_encode($result);
        fwrite($file,$json);
        fclose($file);
        return $result;
    }
}
//- See more at: http://blog.chapagain.com.np/php-how-to-get-stock-quote-data-from-yahoo-finance-complete-code-and-tutorial/#sthash.XmzC3rZM.dpuf

?>