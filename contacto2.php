<?php
$nombre = $_POST['nombre'];
$empresa = $_POST['empresa'];
$correo = $_POST['email'];
$telefono = $_POST['telefono'];
$comentario = $_POST['comentario'];

$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=utf-8\r\n";

$mensaje = "
Formulario de contacto desde sitio web GIMPSA .    
<br/><br/>
    Nombre: {$nombre} <br/>
	Empresa: {$empresa} <br/>
    Correo: {$correo} <br/>
	Telefono: {$telefono} <br/>
    Comentario: {$comentario}";


$para = array(
	"rafael@gimpsa.com.mx",
	"magda@gimpsa.com.mx",
	"diana.zendejas@gimpsa.com.mx",
	"jlopez@isb-cc.com",        
	"esanchez@isb-cc.com",
	"fabiola.zamarripa@gimpsa.com.mx",
	"smendoza@gimpsa.com.mx",
	"renesol@prodigy.net.mx",
                  "cguerra@gimpsa.com.mx"
); 

/*
$para = array(
	"esanchez@isb-cc.com",		  
	"ogarcia@1csb.com.mx",
	"cgutierrez@1csb.com.mx",
);*/

require_once 'swift_mailer/swift_required.php';
$message = \Swift_Message::newInstance()

		  // Give the message a subject
		  ->setSubject('Contacto desde sitio web GIMPSA')

		  // Set the From address with an associative array
    		->setFrom(array('contacto@gimpsaelectrical.com' => 'Gimpsa.com'))

		  // Set the To addresses with an associative array
		  ->setTo($para)

		  // And optionally an alternative body
		  ->addPart($mensaje, 'text/html')
		  ;

		// Create the Transport
		$transport = \Swift_SmtpTransport::newInstance('gimpsaelectrical.com', 25)
		  ->setUsername('contacto@gimpsaelectrical.com')
		  ->setPassword('C0nt4ct0*')
		  ;

		$mailer = \Swift_Mailer::newInstance($transport);
		$result = $mailer->send($message);


/*
$para = "contacto@gimpsaelectrical.com";
mail($para,"Gimpsaelectrical.com",$mensaje,$headers);
$para = "donking@gimpsa.com.mx";
mail($para,"Gimpsaelectrical.com",$mensaje,$headers);
$para = "mav@gimpsa.com.mx";
mail($para,"Gimpsaelectrical.com",$mensaje,$headers);
$para = "cguerra@gimpsa.com.mx";
mail($para,"Gimpsaelectrical.com",$mensaje,$headers);
*/
header("location: exito");
?>