<div id="centralContainer">	
	
    <div class="container-content">
    
      	<div class="semsa-content-box">
            	<div class="gradient-top">
                	<h1>Grupo Internacional de Metales Preciosos S.A. de C.V.</h1>
                </div>
        		
                <p>Inicia sus operaciones el 12 de julio de 1988, en la Ciudad de M&eacute;xico con el fin de atender el mercado nacional de metales preciosos operando transacciones de importaci&oacute;n, exportaci&oacute;n y su comercio en general</p>
                
                <span><img src="/img/imgs/logo-semsa.png" class="imageSempsa" /></span>
    
    			<p>GIMPSA es el principal comercializador de oro y plata en la Rep&uacute;blica Mexicana cubriendo m&aacute;s del 80% de este mercado, manteniendo el  liderazgo, gracias al continuo esfuerzo en mejorar su infraestructura, materia humana y financiera, enfocados siempre en ofrecer a nuestros clientes y proveedores un servicio eficiente dentro de un clima de confiabilidad total</p>    
    			
                
        </div>

    
    
        <div class="calidad-content-box">
            <div class="gradient-top">    
                <h1>POLITICA DE CALIDAD</h1>
            </div>    
                
            <p>En SOLUCIONES ECOL&Oacute;GICAS EN METALES, S.A. DE C.V., trabajamos con independencia, imparcialidad  e integridad, cumpliendo con lo establecido en el Sistema de Gesti&oacute;n de Calidad implementado bajo la norma NMX-EC-17025-IMNC-2006 ,  que nos compromete a alcanzar la excelencia , a mejorar diariamente la buena practica profesional, as&iacute; como la calidad de nuestros servicios.
        
        Somos una empresa que contribuye al mejoramiento del medio ambiente, proporcionando servicios de recuperaci&oacute;n y an&aacute;lisis de metales preciosos, cuidamos hasta el mas m&iacute;nimo detalle con la finalidad de servir eficientemente a cada uno de nuestros clientes, brind&aacute;ndoles un servicio profesional acorde a sus necesidades tomando en cuenta que ellos son lo mas importante.</p>
        </div>
        
        <div class="objetivos-content-box">
            <div class="gradient-top"> 
                <h1>OBJETIVOS</h1>
            </div>
            
            <p><strong>Cumpliendo los siguientes objetivos de Calidad:</strong></p>
            
            <ul>
                <li>Mantener resultados satisfactorios del cliente mayor a 85%.</li>
                <li>Tener m&aacute;ximo 4 quejas al mes.</li>
                <li>Entregar a tiempo al menos el 96% de los trabajos encomendados, a partir del ingreso al Laboratorio, considerando lo siguiente:</li>    
            </ul>
            
            <table class="design">
                <tr>
                    <th>Matriz</th>
                    <th>Tiempo de entrega</th>
                </tr>
                
                <tr>
                    <td>Met&aacute;licos</td>
                    <td>3 d&iacute;as</td>
                </tr>
                
                <tr>
                    <td>Tierra y disoluciones</td>
                    <td>15 d&iacute;as</td>
                </tr>    
            </table>
        </div>
    
    	
        <div class="clear"></div>
    	
    
        <div class="mision-content-box">
            <div class="gradient-top"> 
                <h1>MISI&Oacute;N</h1>
            </div>
                    
            <p>El servir a nuestros clientes con atenci&oacute;n, materiales, productos y servicios de la m&aacute;s alta calidad, propiciando una relaci&oacute;n de negocio de largo plazo en un ambiente de total confiabilidad</p>
        </div>
        
        <div class="vision-content-box">
            <div class="gradient-top">     
                <h1>VISI&Oacute;N</h1>
            </div>
            
            <p>Ser l&iacute;der internacional en la recuperaci&oacute;n de metales preciosos.</p>
        </div>    
        
        <div class="semsa-qs-content-box">
            <div class="gradient-top"> 
                <h1>SEMSA</h1>
            </div>
            
            <p>Como resultado de la preocupaci&oacute;n en los temas ambientales de nuestro pa&iacute;s, Grupo IMPSA  en el año de 2001 forma la empresa “Soluciones Ecol&oacute;gicas en Metales S.A de C.V.” (SEMSA)  para ofrecer soluciones a los procesos contaminantes provenientes de la industria mexicana.</p>
            
            <p>Cuenta con dos sucursales que se encuentran en:</p>
            
            <ul style="font-size:11px;">
                <li><i>Calle Madero No. 60, Centro de la Ciudad de M&eacute;xico.</i></li>
                <li><i>Mariano Otero No. 1329, Local 5-s, World Trade Center de Guadalajara</i></li>
            </ul>                
        </div>    

	</div>
            
</div>

<div class="clear" style="margin-bottom:50px;"></div>