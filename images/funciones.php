<?php 
include_once('class.yahoostock.php');
header('Content-Type: text/html');

	function head(){
		echo("
			 
	<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />		 
	<link href='/img/iconG.png' rel='shortcut icon' type='image/x-icon'>
	<link rel='stylesheet/less' type='text/css' href='/css/estilos.less'/>
    <link rel='stylesheet' type='text/css' href='/css/estilos.css' />
    <link rel='stylesheet' type='text/css' href='/popUps/css/sexylightbox.css' media='all' />
	<link href='http://fonts.googleapis.com/css?family=Great+Vibes|Cinzel|Monsieur+La+Doulaise' rel='stylesheet' type='text/css' />
	
    <script src='/js/less-1.3.3.min.js'></script>
	<script src='http://code.jquery.com/jquery-1.9.1.js'></script>
 	<script src='http://code.jquery.com/ui/1.10.3/jquery-ui.js'></script>
	<script src='/js/funciones.js'></script>
	<!-- PopUp Mootools -->
	<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/mootools/1.2.3/mootools-yui-compressed.js'></script>
	<script type='text/javascript' src='/popUps/js/sexylightbox.v2.3.mootools.min.js'></script>
    <script type='text/javascript' src='/popUps/js/sexylightbox.js'></script>
    
    <!--jCarousel-->
    
	<link rel='stylesheet' type='text/css' href='/css/skin.css'>
    <script type='text/javascript' src='/js/jquery.jcarousel.min.js'></script>
			 
			 ");
		}

	function cabecera(){
		echo("
			<div id='menuOculto'>
				<ul>
					<li><a href='http://www.gimpsa.com.mx/' title='Corporativo GIMPSA'>GIMPSA</a></li>
					<li><a href='http://1csb.mx/' title='Call Center'>UNO</a></li>
					<li><a href='#' title='Soluciones Ecológicas en metales'>SEMSA</a></li>
					<li><a href='http://www.prendal.com/' title='Casa de empeño'>PRENDAL</a></li>
				</ul>
			</div>

			<div class='clear'></div>
			
			<div id='backgroundTop'>
				<div id='topContainer'>
					<div id='logo'><a href='/index.php' title='Gimpsa Electrical'>
					<img src='/img/logo.png' /></a></div>
					<span id='btnArrow'>
						<a href='#' id='abrirMenuOculto' title='Corporativo GIMPSA'></a>
					</span>
					
					 <marquee scrollamount=2 scrolldelay=10>
					 <!--<img src='img/marquee.png' height='19' width='837' />-->
					 
					 ");

						$objYahooStock = new YahooStock;
						$objYahooStock->addFormat("snl1d1t1cv");
						
						//Precios metales preciosos ORO; COBRE; PALADIO; PLATINO; PLATA
						$objYahooStock->addStock("GCU13.CMX");
						$objYahooStock->addStock("HGU13.CMX");
						$objYahooStock->addStock("PAU13.NYM");
						$objYahooStock->addStock("PLU13.NYM");
						$objYahooStock->addStock("SIU13.CMX");
						
						foreach( $objYahooStock->getQuotes() as $code => $stock)
						{
							?>
                            Precio actual: <?php echo '&nbsp;'.$stock[1].'&nbsp;'; ?> 
							
							$<?php echo $stock[2]; ?>&nbsp; &nbsp; | &nbsp; &nbsp;
							<?php
						}
					 
					 
					 echo("
					 </marquee>
					
				</div>	
			</div>
			<div class='cromo'></div>
			
			
		");
	}
	
	function menu(){
		echo("
			<div id='menu'>
                        <ul>
                          <li class='nivel1'><a href='/index.php' class='nivel1'>Inicio</a></li>
                          <li class='nivel1'><a href='/quienesSomos.php' class='nivel1'>Quienes Somos</a></li>
                          <li class='nivel1'><a href='/productos.php' class='nivel1'>Productos</a>
                                <ul class='nivel2'>
                                    <li><a href='/productos-electricos.php'>Contactos Eléctricos</a></li>
                                    <li><a href='/productos-decorativos.php'>Productos Decorativos</a></li>
                                </ul>
                          </li>
                          <li class='nivel1'><a href='/temas-de-interes.php' class='nivel1'>Temas de Interés</a></li>
                          <li class='nivel1'><a href='/galeria.php' class='nivel1'>Galer&iacute;a</a></li>
						  <li class='nivel1'><a href='/contacto.php' class='nivel1'>Contacto</a></li>
						  <li class='nivel1'><a href='#' class='nivel1'>&nbsp;</a></li>
						  
                          <li class='nivel1'><a href='#' class='nivel1'>English Version</a></li>
                        </ul>
			</div>    
			
		");
	}
	
	function pie(){
		echo("
		<div class='clear'></div>
		<div class='cromo'></div>
		<div id='backgroundBottom'>
			<div id='bottomContainer'>
				
				
				<div id='containerLeft'>
					<h3>Otros sitios de GIMPSA</h3>
						<ul>
							<li><a href='http://www.gimpsa.com.mx/' target='_blank'>GIMPSA <br /><span>(Corporativo)</span></a></li>
							<li><a href='http://1csb.mx/' target='_blank'>UNO <br /><span>(Contact centers)</span></a></li>
							<li><a href='#' target='_blank'>SEMSA <br /><span>(Soluciones <br />Ecológicas en Metales)</span></a></li>
							<li><a href='http://www.prendal.com/' target='_blank'>PRENDAL <br /><span>Casa de Empeño</span></a></li>
						</ul>
				</div>	
					
				
				<div id='containerRight'>
					<h3>Enlaces de Interes</h3>
						<ul>
							<li>
								<ul>
									<li>GOBIERNO</li>
									<li><a href='http://www.economia.gob.mx/comunidad-negocios/mineria' target='_blank'><span>Sría. Economia (Miner&iacute;a)</span></a></li>
									<li><a href='http://www.sgm.gob.mx/' title='Servicio Geológico Mexicano' target='_blank'><span>S.G.M</span></a></li>
								</ul>	
							</li>

							<li>
								<ul>
									<li>PUBLICACIONES</li>
									<li><a href='http://www.oroyfinanzas.com/'><span>Oro y Finanzas</span></a></li>
									<li><a href='http://www.kitcometals.com/charts/' target='_blank' title='Precios de metales en el mercado'><span>KITKO</span></a></li>
								</ul>
							</li>
							
							<li>
								<ul>
									<li>ASOCIACIONES</li>
									<li><a href='http://www.geomin.com.mx/' target='_blank' title='Asociación de Ingenieros de Minas, Metalurgistas y Geólogos de México, A.C.'><span>A.I.M.M.G.M</span></a></li>
								</ul>
							</li>
							
							<li style='margin-top:4px;'>
								<ul>
									<li>
						<a href='/popUps/iso_9001.html?TB_iframe=true&amp;height=365amp;width=460&amp;scrolling=no;' title='EMPRESA CERTIFICADA ISO-9001' rel='sexylightbox' class='aviso'><span class='iso'></span></a></li>
								</ul>
							</li>

						</ul>
				</div>		
				<div class='clear'></div>
						
						<p>
							</a>
							<a href='/popUps/avisoPrivacidad.html?TB_iframe=true&amp;height=280&amp;width=500&amp;scrolling=no;' title='Aviso de Privacidad' rel='sexylightbox' class='aviso'>Aviso de privacidad</a>
							Rio Lerma #333, Col. Cuahut&eacute;moc D.F. C.P. 65000  | Tel&eacute;fono (52) 5121-1892 y (01-800) 022-0499	 &nbsp; &nbsp; | &nbsp; &nbsp;
							<span> © 2013 Todos los derechos reservados</span> 
						</p>
						
						
						<img src='/img/divBottom.jpg' width='2' height='93' border='0' class='divBottom' />
						
					
			</div>
		</div>
		");
	}
	
	
	
	function indicadores(){
		 
	$objYahooStock = new YahooStock;
	$objStockMarket = new YahooStock;
	$objWordMarkets = new YahooStock;
	
	$objYahooStock->addFormat("snl1d1t1cv");
	$objStockMarket->addFormat("snl1d1t1cv");
	$objWordMarkets->addFormat("snl1d1t1cv");
	
	//Mercado Cambiario
	$objYahooStock->addStock("USDMXN=X");
	$objYahooStock->addStock("EURMXN=X");
	$objYahooStock->addStock("GBPMXN=X");
	$objYahooStock->addStock("JPYMXN=X");


	$tabla = "	<div id='rightContainer'>
				<h2>Indicadores Financieros</h2>
				<div id='containerIndicators'>
				<div class='indicadores'>
				
				<p>Mercado Cambiario</p>
				<table summary='Mercado de divisas'>
		
				<thead>
					<tr>
						<th>Moneda</th>
						<th>Precio</th>
						<th>Actual</th>
					</tr>
				</thead>
				
				<tfoot>
					<tr>
						<td colspan='3'> &nbsp;</td>
					</tr>
				</tfoot>
				
				<tbody>
				";
				
	foreach( $objYahooStock->getQuotes() as $code => $stock)
	{
		if($code == 'USDMXN=X') $code = 'Dolar:';
		if($code == 'EURMXN=X') $code = 'Euro:';
		if($code == 'GBPMXN=X') $code = 'Libra(ing):';
		if($code == 'JPYMXN=X') $code = 'Yen:';
		
		$tabla.= '<tr><td>'.$code.'</td>';
		$tabla.= '<td>'.$stock[2].'</td>';
		$tabla.= '<td>'.$stock[3].'</tr>';
	}
	
	$tabla.= '</tbody></table></div>';

	echo $tabla;

		//Mercados Mundiales
	$objWordMarkets->addStock("BOLSAA.MX");
	$objWordMarkets->addStock("^BVSP");
	$objWordMarkets->addStock("^IPSA");
	$objWordMarkets->addStock("^MERV");
	
	$tabla2 = "
		<div class='indicadores'>
		<p class='worldMarket'>Mercados Mundiales</p>					
			<table>
				<thead>
					<tr>
						<th>Mercado</th>
						<th>Precios</th>
						<th>Variaci&oacute;n</th>
					</tr>
				<thead>			
			<tbody>";
			
	foreach($objWordMarkets->getQuotes() as $code => $stock)
	{
		$tabla2.= '<tr><td>'.$stock[1].'</td>';
		$tabla2.= '<td>'.$stock[2].'</td>';
		$tabla2.= '<td>'.$stock[5].'</tr>';
	}
		
	$tabla2.= '</tbody></table></div>';
	
	echo $tabla2;
	
	
	//Mercado accionario
	$objStockMarket->addStock("^MXX");
	$objStockMarket->addStock("IRTCOMPMX.MX");
	
	$tabla3 = "
		<div class='indicadores'>
		<p class='stockMarket'>Mercado Accionario</p>					
			<table>
				<thead>
					<tr>
						<th>&Iacute;ndices</th>
						<th>Precios</th>
						<th>Variaci&oacute;n</th>
					</tr>
				<thead>			
			<tbody>";
			
	foreach($objStockMarket->getQuotes() as $code => $stock)
	{
		$tabla3.= '<tr><td>'.$stock[1].'</td>';
		$tabla3.= '<td>'.$stock[2].'</td>';
		$tabla3.= '<td>'.$stock[5].'</tr>';
	}
		
	$tabla3.= '</tbody></table></div></div></div>';
	
	echo $tabla3;
	
	} 
		




	function news(){
	$long_descripcion=100;  
	$num_noticias=5;  
	$n=0;
	echo ("
	<div class='conteinerNews'>
	<h2>Noticias</h2>");
	$noticias = simplexml_load_file('http://www.eluniversal.com.mx/rss/finanzas.xml');
	foreach ($noticias as $noticia) {   
		foreach($noticia as $reg){  
			if($reg->title!=NULL && $reg->title!='' && $reg->description!=NULL && $reg->description!='' && $n<$num_noticias){  
				echo "
					<h3><a href='/".$reg->link."' target='_blank'>".$reg->title."</a></h3>";  
				if(strlen($reg->description)>$long_descripcion){ 
					echo "<p>".substr($reg->description,0,$long_descripcion)."...</p>"; 
					echo "<p>Publicado:".$reg->pubDate."</p>"; 
				}
				else{  
					echo "
						<p>".$reg->description."</p>";  
					echo "
						<p>Publicado:".$reg->pubDate."</p>"; 
				} 
				$n++;  
			}  
		}  
	}   
	echo("
		 <a href='http://www.eluniversal.com.mx/finanzas-cartera/'>
		 <img src='/img/fuenteEluniversal.png' width='170' height='32' border='0' /></a>
		 </div>");
		}
?>