<link rel='stylesheet' type='text/css' href='/css/skin.css'>
<div id="centralContainer">	


	<h1>CAT&Aacute;LOGO DE PRODUCTOS</h1> 
	<div id="contenedorProducto">
    	
        <div class="contenedorProducto_descripcion"> 
                  
                  		<img src="/img/imgProductos/carbonato_de_plata.jpg" title="Carbonato de plata" class="queryImg-productos" />
                  
                        <ul> 
                            <li class="categorias"  style="width:80px;">Productos</li> 
                            <li class="categorias" style="width:110px;">Carbonato de Plata</li>
                        </ul> 
                          
                     	<h2>Carbonato de Plata</h2> 
                              
                      	<h3>Descripci&oacute;n del producto</h3> 

                       	<p>El carbonato de plata es un compuesto qu&iacute;mico con la f&oacute;rmula Ag2C03. Esta sal es de color amarillo pero las muestra t&iacute;picas son gris&aacute;ceas debido a la precencia de plata elemental y es altamente fotosensible</p>
                            
                        <h3 class="not-visible">Aplicaciones</h3> 
                        
                        <p class="not-visible">Como reactivo en s&iacute;ntesis org&aacute;nica para la elaboraci&oacute;n de pastas met&aacute;licas, remoci&oacute;n de cloro disuelto en agua, preparaci&oacute;n para pel&iacute;culas en fotograf&iacute;a , elaboraci&oacute;n de pol&iacute;meros altamente conductores, tratamientos dentales, agente biol&oacute;gico, etc.</p>    
                        
                        <h3 class="not-visible">Presentaci&oacute;n</h3>
                        
                        <ul class="not-visible" style="border:none;">
                        	<li>Polvo de 100 y 200 mallas.</li>
                            <li>Cotizaci&oacute;n a partir de 100 g</li>
                        </ul>    
                       
                       	<div class="clear"></div>
                        
                        <a href="contacto" title="Contacta con nosotros" class="contacta">Cont&aacute;ctanos</a>
                        <a title="M&aacute;s informaci&oacute;n" class="contacta" onclick="mostrar();">M&aacute;s informaci&oacute;n</a></span>                  
                		
                          
		</div>
                  
        		<img src="/img/imgProductos/carbonato_de_plata.jpg" title="Carbonato de plata" class="imgsProductos"/>
		          
                  
                  <div class="atencionCliente">
                      <p><strong>Atenci&oacute;n al cliente</strong></p>
                      <img src="/img/atencioCliente.jpg" width="47" height="47" />
                      <span>DF (52) 5121-1892</span>
                      <span>Lunes a viernes 7am a 4pm</span>
                  </div>
                  
                  <div id='oculto'>          
                  		<h3>Composici&oacute;n</h3>
                      <p>72 a 75 % Ag, 25 % CO3</p>
                  </div>                         
    </div>          
    
    		<?php include('contenedor_lateral_derecho_productos.php');?>
    
			<div class="clear"></div>
			
            <div class="jcarousel-skin-tango" style="margin:40px 0 40px 0; width:80%">
                      <h2>Otros productos que te podr&iacute;an interesar</h2>
                        
                        <div class="jcarousel-clip jcarousel-clip-horizontal">
                          <ul id="mycarousel" style="overflow: hidden; position: relative; top: 0px; margin: 0px; padding: 0px; left: 0px;">
                                
                            <li>
                                <a href="resinato-de-oro" title="Resinato de oro" class="resinato-de-oro">
                                Resinato de oro</a></li>
                                
                            <li>
                                <a href="cloruro-de-oro" title="Cloruro de oro" class="cloruro-de-oro">
                                Cloruro de oro</a></li>
                            
                            <li>
                                <a href="cloruro-de-plata" title="Cloruro de plata" class="cloruro-de-plata">
                                Cloruro de plata</a></li>
                 
                            <li>
                                <a href="nitrato-de-plata" title="Nitrato de plata" class="nitrato-de-plata">
                                Nitrato de plata</a></li>
                                
                            <li>
                                <a href="polvo-de-plata" title="Polvo de plata" class="polvo-de-plata">
                                Polvo de plata</a></li>
                                	 
                            <li>
                                <a href="granallado-de-plata" title="Granallado de plata" class="granallado-de-plata">
                                Granallado de plata</a></li>
                            
                            <li>
                                <a href="pasta-de-oro-brillante" title="Pasta de oro brillante" class="pasta-de-oro-brillante">
                                Pasta de oro brillante </a></li>
                            
                            <li>
                                <a href="pasta-de-platino-brillate" title="Pasta de platino brillante" class="pasta-de-platino-brillante">
                                Pasta de platino brillante</a></li>
                            
                            <li>
                                <a href="oro-liquido-brillante" title="Oro l&iacute;quido brillante" class="oro-liquido-brillante">
                                Oro l&iacute;quido brillante</a></li>
                                
                            <li>
                                <a href="pasta-de-oro" title="Pasta de oro" class="pasta-de-oro">
                                Pasta de oro</a></li>
                                
                            <li>
                                <a href="platino-liquido-brillante" title="Platino l&iacute;quido brillante" class="platino-liquido-brillante">
                                Platino l&iacute;quido brillante</a></li>
                                
                            <li>
                                <a href="pasta-de-platino-brillante-ceramica" title="Pasta de platino brillante cer&aacute;mica" class="pasta-de-platino-brillante-ceramica">
                                Pasta de platino brillante cer&aacute;mica</a></li>                                  
                                                            
                                                            
                                                            
                                
                          </ul>
                      </div>
                      
                </div>
            
</div>          