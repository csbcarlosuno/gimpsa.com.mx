<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento no encontrado</title>
<style type="text/css">
    body{margin: 0;}
    .fondo404{ background: url(../img/404.jpg) no-repeat center; width: 100%; height: 960px; margin: 0 auto;}
    h2{ position: absolute; top: 327px; left: 20%; color: #585946; font-family: Helvetica; }
    h2 span{ font-size: 1.2em;}
    h2 span+span{ font-style:italic  }
    img { margin-top: 40px;}
    
</style>
</head>

<body>
    
    <div class="fondo404">
	
        <h2><span>La página que buscas no existe !!<br/></span>
            <span>Prueba siguiendo este enlace:</span> <a href="/inicio" title="HOME"><img src="../img/404-logo.png" /></a></h2>
                
    </div>
</body>
</html>