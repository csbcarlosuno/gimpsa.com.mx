	       	
<div id="centralContainer">	
	
	<div class="content-products">
        
        <h2>PRODUCTOS</h2>
        
        
          <ul class="grid" style="position:relative;">
          
          	  <li class="topic topic-mobile" style="border-radius:16px;">
                  <a href="granallado.php" title="GIMPSA "><span class="icon-topic icon-pasta">&nbsp;</span>
                  <h2>
                  Granallado
                  </h2>
                  
                  <ul>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                  </ul>
                  <span class="icon-more">&nbsp;</span> 
                  <span class="btn-view-all maia-button maia-button-secondary">M&aacute;s informaci&oacute;n</span></a>
              </li> 
              
              <li class="topic topic-mobile" style="border-radius:16px;">
                  <a href="laminado.php" title="GIMPSA "><span class="icon-topic icon-pasta">&nbsp;</span>
                  <h2>
                  Laminado
                  </h2>
                  
                  <ul>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                  </ul>
                  <span class="icon-more">&nbsp;</span> 
                  <span class="btn-view-all maia-button maia-button-secondary">M&aacute;s informaci&oacute;n</span></a>
              </li> 
              
              <li class="topic topic-mobile" style="border-radius:16px;">
                  <a href='pastas-para-decoracion' title="GIMPSA ">
                  <span class="icon-topic icon-pastaCeramica">&nbsp;</span>
                  <h2>
                  Fabricaci&oacute;n de pasta de oro y platino para la impresi&oacute;n de cerigraf&iacute;a en vidio y cer&aacute;mica
                  </h2>
                  
                  <ul>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                  </ul>
                  <span class="icon-more">&nbsp;</span> 
                  <span class="btn-view-all maia-button maia-button-secondary">M&aacute;s informaci&oacute;n</span></a>
              </li>
          
              <li class="topic topic-mobile" style="border-radius:16px;">
                  <a href="nitrato-de-alta-pureza.php" title="GIMPSA ">
                  <span class="icon-topic icon-pastaVidrio">&nbsp;</span>
                  <h2>
                  Nitrato de alta pureza
                  </h2>
              	  
                  <ul>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                  </ul>
				  
                  <span class="icon-more">&nbsp;</span> 
                  <span class="btn-view-all maia-button maia-button-secondary">M&aacute;s informaci&oacute;n</span>
				  
				  </a>
              </li>
              
               <li class="topic topic-mobile" style="border-radius:16px;">
                  <a href="cloruro-de-plata.php" title="GIMPSA "><span class="icon-topic icon-pasta">&nbsp;</span>
                  <h2>
                  Cloruro de plata
                  </h2>
                  
                  <ul>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                  </ul>
                  <span class="icon-more">&nbsp;</span> 
                  <span class="btn-view-all maia-button maia-button-secondary">M&aacute;s informaci&oacute;n</span></a>
              </li>
              
              
              
              <li class="topic" style="border-radius:16px;">
                  <a href="polvo-de-oro-y-plata-de-alta-pureza.php" title="GIMPSA ">
                  <span class="icon-topic icon_esmalteVitreo">&nbsp;</span>
                  <h2>
                  Polvo de oro y plata de alta pureza
                  </h2>
                  
                  <ul>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                  </ul>
                  <span class="icon-more">&nbsp;</span> 
                  <span class="btn-view-all maia-button maia-button-secondary">M&aacute;s informaci&oacute;n</span></a>
              </li>
              
              <li class="topic" style="border-radius:16px;">
                  <a href="fabricacion-de-oro-liquido-y-platino-para-vidrio-y-ceramica.php" title="GIMPSA ">
                  <span class="icon-topic icon_esmalteCeramico">&nbsp;</span>
                  <h2>
                  Fabricaci&oacute;n de oro y l&iacute;quido y platino para vidrio y cer&aacute;mica
                  </h2>
                  
                  <ul>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li> 
                      <li>&nbsp;</li>
                  </ul>
                  <span class="icon-more">&nbsp;</span> 
                  <span class="btn-view-all maia-button maia-button-secondary">M&aacute;s informaci&oacute;n</span></a>
              </li>
              
              <li class="topic" style="border-radius:16px;">
                  <a href="fabricacion-de-carbonato-plata.php" title="GIMPSA ">
                  <span class="icon-topic icon-pintado">&nbsp;</span>
                  <h2>
                  Fabricaci&oacute;n de carbonato de plata
                  </h2>
                  
                  <ul>
                  	  <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                  </ul>
                  <span class="icon-more">&nbsp;</span> 
                  <span class="btn-view-all maia-button maia-button-secondary">M&aacute;s informaci&oacute;n</span></a>
              </li>             
                
              <li class="topic topic-mobile" style="border-radius:16px;">
                  <a href="plata-para-afinacion.php" title="GIMPSA ">
                  <span class="icon-topic">&nbsp;</span>
                  <h2>
                  Dore de plata para afinaci&oacute;n
                  </h2>
              	  
                  <ul>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                      <li>&nbsp;</li>
                  </ul>
				  
                  <span class="icon-more">&nbsp;</span> 
                  <span class="btn-view-all maia-button maia-button-secondary">M&aacute;s informaci&oacute;n</span>
				  
				  </a>
              </li>                                                                      
                
          </ul>
        
	</div>
</div>
