<?php


if(isset($_GET['section'])){
	switch ($_GET['section']) {
		case 'index':
			include('inicio.php');
			break;
		case 'quienes-somos':
			include('quienes-somos.php');
			break;
		case 'productos':
			include('productos.php');
			break;	
		case 'resinato-de-oro':
			include('resinato-de-oro.php');
			break;	
		case 'cloruro-de-oro':
			include('cloruro-de-oro.php');
			break;	
		case 'cloruro-de-plata':
			include('cloruro-de-plata.php');
			break;	
		case 'nitrato-de-plata':
			include('nitrato-de-plata.php');
			break;	
		case 'polvo-de-plata':
			include('polvo-de-plata.php');
			break;	
		case 'granallado-de-plata':
			include('granallado-de-plata.php');
			break;	
		case 'carbonato-de-plata':
			include('carbonato-de-plata.php');
			break;	
		case 'pasta-de-platino-brillante':
			include('pasta-de-platino-brillante.php');
			break;	
		case 'oro-liquido-brillante':
			include('oro-liquido-brillante.php');
			break;	
		case 'pasta-de-oro':
			include('pasta-de-oro.php');
			break;	
		case 'platino-liquido-brillante':
			include('platino-liquido-brillante.php');
			break;	
		case 'pasta-de-platino-brillante-ceramica':
			include('pasta-de-platino-brillante-ceramica.php');
			break;										
		case 'pasta-de-oro-brillante':
			include('pasta-de-oro-brillante.php');
			break;
			
		case 'comercio':
			include('comercio.php');
			break;
		case 'compra-de-metales':
			include('compra-de-metales.php');
			break;
		case 'venta-de-metales':
			include('venta-de-metales.php');
			break;
		case 'transporte-de-valores':
			include('transporte-de-valores.php');
			break;
			
		case 'servicios':
			include('servicios.php');
			break;
		case 'analisis-de-tierras':
			include('analisis-de-tierras.php');
			break;
		case 'analisis-metalico':
			include('analisis-metalico.php');
			break;
		case 'analisis-de-joyeria':
			include('analisis-de-joyeria.php');
			break;
		case 'analisis-de-escoria':
			include('analisis-de-escoria.php');
			break;
		case 'analisis-de-mineral':
			include('analisis-de-mineral.php');
			break;
		case 'analisis-de-soluciones':
			include('analisis-de-soluciones.php');
			break;
		case 'refinacion-de-oro':
			include('refinacion-de-oro.php');
			break;
		case 'refinacion-de-plata':
			include('refinacion-de-plata.php');
			break;
		case 'refinacion-de-paladio':
			include('refinacion-de-paladio.php');
			break;
		case 'refinacion-de-platino':
			include('refinacion_de_platino.php');
			break;
		case 'fundicion-metalicos':
			include('fundicion-metalicos.php');
			break;
		case 'fundicion-no-metalicos':
			include('fundicion-no-metalicos.php');
			break;
		case 'laboratorio-tercero':
			include('laboratorio-tercero.php');
			break;
		case 'proceso':
			include('proceso.php');
			break;
			
		case 'contacto':	
			include('contacto.php');
			break;
		case 'exito':	
			include('exito.php');
			break;	
			
		default:
			include('inicio.php');
			break;
	}
}else{
	include('inicio.php');
} 


?>