<div id="centralContainer" style="margin-top:-50px">	
	<div id="containerTraging">
    
    <img src="/img/venta-metales.jpg" style="margin-bottom:35px;" />

	<h2>VENTA DE METALES</h2>
	
    <p>Est&aacute;n abiertas a cualquier cliente previa presentaci&oacute;n de su Registro Federal de Contribuyentes y verificaci&oacute;n de su domicilio fiscal y existencia del mismo.</p>

	<p>El precio se determina en base a las cotizaciones en d&oacute;lares americanos por onza troy y podr&aacute;n ser pactadas al precio Spot del momento en Nueva York. Para el oro tambi&eacute;n podr&aacute; utilizarse el precio del Cierre de Londres (2º. Fix). En este caso la orden del cliente deber&aacute; ser el d&iacute;a anterior al de la venta. Los productos podr&aacute;n ser entregados en el domicilio del cliente dependiendo del vol&uacute;men y precio pactado.</p>

	<p>El pago de los productos podr&aacute; ser en d&oacute;lares americanos o en pesos mexicanos al tipo de cambio, con cheque certificado o de caja, v&iacute;a SPEUA o deposito bancario. Antes de la entrega del metal, el pago deber&aacute; estar registrado en nuestra cuenta.</p>

	<p>Recomendamos que los pagos en d&oacute;lares sean en documento, ya que en caso de hacerlo con efectivo, los bancos cobran comisiones demasiado altas. Para contrarrestar este problema, apoyamos a nuestros clientes con tipos de cambio preferenciales, aprovechando nuestras relaciones y capacidad de negociaci&oacute;n con casas de cambio y bancos para la compra de d&oacute;lares, permitiendo as&iacute; que estos depositen en pesos en las cuentas de IMPSA que se les asigne.</p>

	<p>Desafortunadamente a partir de que los bancos decidieron acreditar los pagos de cheques de otros bancos a las cuentas de los beneficiarios a 24 o 48 horas dependiendo de la hora, provocaron una p&eacute;rdida de agilidad en las compras y ventas del mercado de "ventas al contado" en donde por seguridad la entrega y recibo de los productos, y cierre de negociaciones, depende de que los pagos est&eacute;n acreditados y en firme en las cuentas del proveedor.</p>

	<h2>COBRANZA</h2>
	
    <p>Las ventas son al contado y en t&eacute;rminos generales IMPSA por el momento no otorga cr&eacute;ditos debido a la naturaleza del mercado de estos metales preciosos.</p>

	<p>A fin de dar alternativas a nuestros clientes y que puedan atender necesidades extraordinarias de metales preciosos, actualmente estamos implementando operaciones de seguro de cr&eacute;dito, donde el cliente podr&aacute; recibir el metal, quedando asegurado el pago a favor de IMPSA. Estas operaciones tambi&eacute;n podr&aacute;n ser realizadas por medio de Prime Capital, nuestro asociado financiero.</p>

	<p style="margin-bottom:20px;">Para hacer uso de este cr&eacute;dito, ser&aacute; necesaria la aprobaci&oacute;n de la compañ&iacute;a de seguros. El cliente deber&aacute; presentar sin excepci&oacute;n la siguiente documentaci&oacute;n:</p>
	
    <ul>
		<li>Acta constitutiva</li>
		<li>Estados financieros dictaminados</li>
		<li>Referencias bancarias</li>
		<li>Referencias comerciales</li>
	</ul>
    <div class="clear"></div>
    
	<p style="margin-top:20px;">El monto del cr&eacute;dito siempre ser&aacute; en d&oacute;lares americanos, los pagos podr&aacute;n ser en d&oacute;lares o en Moneda Nacional al tipo de cambio negociado al momento del pago.</p>

	<p>Sugerimos a nuestros clientes manejar sus cuentas bancarias en los mismos bancos de IMPSA.</p>

	<h2>GENERALES</h2>
    	
	<p>A fin de mantener el crecimiento constante de ventas en el mercado nacional y de exportaci&oacute;n, hemos invertido en el aumento de nuestro capital de trabajo e infraestructura, en la medida necesaria para atender a nuestra creciente clientela y aumentar nuestro n&uacute;mero de proveedores confiables.</p>

	<p>Prevenimos cualquier riesgo, asegurando todos nuestros productos en su estancia ya sea en b&oacute;vedas, planta o traslado y por el lado de la comercializaci&oacute;n por medio de pol&iacute;ticas establecidas para los precios, ventas, coberturas y cobranza.</p>
    
    </div>
    <?php include('contenedor_lateral_derecho_trading.php');?>
</div>  