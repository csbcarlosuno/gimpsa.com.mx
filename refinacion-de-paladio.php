<link rel='stylesheet' type='text/css' id='camera-css' href='/css/camera.css' />
<link rel='stylesheet' type='text/css' href='/css/skin.css'>
<script type='text/javascript' src='/camera/scripts/jquery.min.js'></script>
<script type='text/javascript' src='/camera/scripts/jquery.mobile.customized.min.js'></script>
<script type='text/javascript' src='/camera/scripts/jquery.easing.1.3.js'></script> 
<script type='text/javascript' src='/camera/scripts/camera.min.js'></script> 

<div id="centralContainer">	


	<h1>SERVICIOS</h1> 
	<div id="contenedorProducto">
    	
        <div class="contenedorProducto_descripcion"> 
                  
                  		<img src="/img/imgServicios/refinacion_de_paladio.jpg" title="Refinaci&oacute;n de paladio" class="queryImg-productos" />
						
                        
                        
                        <h3>Refinaci&oacute;n de Paladio (Pl).</h3>                  
                  
                        <ul class="services-list"> 
                            <li class="categorias">Joyer&iacute;a</li>  
                            <li class="categorias" style="width:100%">Barra Met&aacute;lica</li>
                            <li class="categorias" style="width:100%">Rebaba</li>
                            <li class="categorias">Vajillas</li>
                            <li class="categorias">Tarjetas Electr&oacute;nicas</li>
                            <li class="categorias">Scrap</li>
                            <li class="categorias">Pedacer&iacute;a</li>
                            <li class="categorias">Dores de Miner&iacute;a</li>
                            <li class="categorias">Tierras</li>
                            <li class="categorias">Escorias</li>
                            <li class="categorias">Minerales</li>
                            <li class="categorias">Barredura</li>
                            <li class="categorias">Pelusas</li>
                            <li class="categorias">Crisoles</li>
                            <li class="categorias">Copelas</li>
                            <li class="categorias">Scrap de Joyer&iacute;a</li>
                            <li class="categorias">Lodos</li>
                        </ul> 

                       	<div class="clear"></div>
                        
                        <a href="contacto" title="Contacta con nosotros" class="contacta">Cont&aacute;ctanos</a>                		
                          
		</div>          
                        
                       <img src="/img/imgServicios/refinacion_de_paladio.jpg" title="Refinaci&oacute;n de paladio" class="imgsProductos" /> 
                        
    					<div class="clear"></div>   

                        <div class="atencionCliente">
                            <p><strong>Atenci&oacute;n al cliente</strong></p>
                            <img src="/img/atencioCliente.jpg" width="47" height="47" />
                            <span>DF (52) 5121-1892</span>
                            <span>Lunes a viernes 7am a 4pm</span>
                       </div>
                                   
    	</div>  
            
   		<?php include('contenedor_lateral_derecho_servicios.php');?>
            
            
            
            
			<div class="clear"></div>		
</div>      