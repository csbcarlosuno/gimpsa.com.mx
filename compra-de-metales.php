<div id="centralContainer" style="margin-top:-50px">		
    <div id="containerTraging">

	<h2>COMPRAS DE METALES FINOS</h2>
    
    <div class="container-compras-oro">

        <p>Las compras de metales preciosos como Oro, Plata, Paladio y Platino, son de acuerdo a la demanda y venta diaria de los mismos. Sin excepci&oacute;n siempre se encuentran protegidas con su respectiva cobertura.</p>
    
        <p>Las cotizaciones para las compras de productos son en d&oacute;lares americanos por onza y se verifican en la pantalla de precios Internacionales conectada al mercado de Nueva York.</p>
    
        <p>El pago de las compras podr&aacute; ser en d&oacute;lares americanos o en pesos mexicanos al tipo de cambio que se establezca en el momento de la operaci&oacute;n.</p>
    
        <p>Todos los pagos son por anticipado y se libera el metal hasta que se registra en nuestras cuentas bancarias el ingreso de los recursos.</p>
        
    </div>
        
    	<img src="/img/compras-oro.jpg" class="compras-oro" />

	<h2 style="margin:20px 10px">COMPRA DE OTROS METALES</h2>
    
    
	
    <p style="margin-bottom:20px;">Con la ventaja de contar con nuestra planta de refinaci&oacute;n de metales, IMPSA puede adquirir productos sin refinar de la industria minera y prestar servicios de maquila a terceros sobre los siguientes productos:</p>

	<ul>
		<li> Minerales de oro y plata de alta ley</li>
		<li> Precipitados de oro y plata</li>
		<li> Metal dore</li>
		<li> L&iacute;quidos con contenidos de oro y plata</li>
		<li> Tierras</li>
		<li> Pelusas</li>
		<li> Polvos</li>
		<li> Lodos</li>
		<li> Chatarra electr&oacute;nica</li>
		<li> Escorias</li>
   </ul>     
   
   		<div class="container-compras-otros-metales">
        	<img src="/img/compras-otros-metales-1.jpg" />
            <img src="/img/compras-otros-metales-2.jpg" />
            <img src="/img/compras-otros-metales-3.jpg" />
        </div>
   
   	<div class="clear"></div>

	<p style="margin-top:20px;">Para las compras de estos productos, previamente se determinan los contenidos recuperables de metales preciosos en el laboratorio de la planta y, dependiendo del contenido de estos y los precios internacionales vigentes, se calcula el valor del material, descontando el porcentaje de merma que ocasionar&aacute; el proceso de afinaci&oacute;n, as&iacute; como el costo de la maquila. La totalidad del pago no exceder&aacute; de cinco d&iacute;as h&aacute;biles. </p>

	</div>
    
		<?php include('contenedor_lateral_derecho_trading.php');?>
        <div class="clear"></div>
</div>