<div id="centralContainer">	
	
    <h2>SERVICIOS</h2>
    
    <div class="cols-lg-3">
    <ul>
    	<li>An&aacute;lisis Qu&iacute;micos para la determinaci&oacute;n de contenidos de Oro (Au), Plata (Ag), Paladio (Pd) y Platino (Pt).</li>
		<li>An&aacute;lisis qu&iacute;micos de tierras y minerales con contenidos de Oro (Au), Plata (Ag), platino (Pt) y paladio (Pt).</li>
		<li>An&aacute;lisis qu&iacute;micos de convertidores catal&iacute;ticos.</li>
		<li>Se certifica la pureza del oro, plata, platino y paladio.</li>
    </ul>
    	
        <img src="/img/imgs/serv1.jpg" width="158" height="123"  />
        <img src="/img/imgs/serv2.jpg" width="158" height="123"  />
    	<hr />
    </div>    
    
    <div class="cols-lg-3">
    <ul>
    	<li>Refinaci&oacute;n de Oro (Au), Plata (Ag), Paladio (Pd) y Platino (Pt).</li>
        <li>Refinaci&oacute;n escorias en pequeño y gran volumen con contenidos de Oro, plata, platino y paladio.</li>
		<li>Refinaci&oacute;n de la tierra y basura con bajos contenidos de Oro (Au), plata (Ag), Platino (Pt) y Paladio (Pd).</li>
		<li>Refinaci&oacute;n de dores con oro (Au) y Plata (Ag) de la miner&iacute;a.</li>
		<li>Refinaci&oacute;n de precipitados de miner&iacute;a de oro (Au) y plata (Ag).</li>
    </ul>
    
    	<img src="/img/imgs/serv3.jpg" width="158" height="123"  />
        <img src="/img/imgs/serv4.jpg" width="158" height="123"  />    	
        <hr />
    </div>    
        
    <div class="cols-lg-3">    
    <ul>
    	<li>Recuperaci&oacute;n de metales preciosos de l&iacute;quidos y soluciones.</li>
        <li>Granallado y laminado de metales preciosos.</li>
		<li>Compra y venta de metales preciosos.</li>
		<li>Compra de catalizadores automotrices e industriales.</li>
		<li>Compramos chatarra con oro, plata, platino y paladio.</li>
		<li>Compramos escoria con contenidos en oro y plata.</li>
		<li>Compra de barros y tierras con contenidos de oro y plata.</li>
		<li>Compra de Scrap o Material Impregnado de Oro (Au) y Plata (Ag).</li>	
    </ul>
    
    	<img src="/img/imgs/serv5.jpg" width="158" height="123"  />
        <img src="/img/imgs/serv6.jpg" width="158" height="123"  />
        <hr />
    </div>
    
</div>
