	<link rel='stylesheet' type='text/css' href='/css/skin.css'>
    <!-- Carousel respons-->
    <link rel='stylesheet' type='text/css' id='camera-css' href='/css/camera.css' />
	<script type='text/javascript' src='/camera/scripts/jquery.min.js'></script>
    <script type='text/javascript' src='/camera/scripts/jquery.mobile.customized.min.js'></script>
    <script type='text/javascript' src='/camera/scripts/jquery.easing.1.3.js'></script>
    <script type='text/javascript' src='/camera/scripts/camera.min.js'></script>

<div id="centralContainer">

    <script type="text/javascript">
     jQuery(document).ready(function() {
       jQuery("#slideCamera").camera({
         height: '31%',
         thumbnails: true,
       });
     });

    </script>

    <div class="fluid_container" style="width:90%; margin:0 auto;">
        <div class="camera_wrap camera_azure_skin" id="slideCamera">
            <div data-thumb="/img/gimpsaslides/thumbs/gimpsaslides1.jpg" data-src="/img/gimpsaslides/gimpsaslides1.jpg"></div>
            <div data-thumb="/img/gimpsaslides/thumbs/gimpsaslides2.jpg" data-src="/img/gimpsaslides/gimpsaslides2.jpg"></div>
            <div data-thumb="/img/gimpsaslides/thumbs/gimpsaslides3.jpg" data-src="/img/gimpsaslides/gimpsaslides3.jpg"></div>
            <div data-thumb="/img/gimpsaslides/thumbs/gimpsaslides4.jpg" data-src="/img/gimpsaslides/gimpsaslides4.jpg"></div>
            <div data-thumb="/img/gimpsaslides/thumbs/gimpsaslides5.jpg" data-src="/img/gimpsaslides/gimpsaslides5.jpg"></div>
            <div data-thumb="/img/gimpsaslides/thumbs/gimpsaslides6.jpg" data-src="/img/gimpsaslides/gimpsaslides6.jpg"></div>
        </div>
    </div>

    <div class="clear"></div>


        		<script type="text/javascript">
				  $(document).ready(function() {

				   $('#carouselDecorative').jcarousel({
						scroll: 1,
						visible: 6,
						wrap: 'circular'
					  });
				 });
				</script>


                <!-- Carrusel para productos decorativos -->

              	<div class="jcarousel-skin-tango" style="margin:40px 0 40px 0;">
                      <h2>Productos</h2>

                        <div class="jcarousel-clip jcarousel-clip-horizontal">
                          <ul id="carouselDecorative" style="overflow: hidden; position: relative; top: 0px; margin: 0px; padding: 0px; left: 0px; width: 950px;">

                            <li>
                                <a href="resinato-de-oro" title="Resinato de oro" class="resinato-de-oro">
                                Resinato de oro</a></li>

                            <li>
                                <a href="cloruro-de-oro" title="Cloruro de oro" class="cloruro-de-oro">
                                Cloruro de oro</a></li>

                            <li>
                                <a href="cloruro-de-plata" title="Cloruro de plata" class="cloruro-de-plata">
                                Cloruro de plata</a></li>

                            <li>
                                <a href="nitrato-de-plata" title="Nitrato de plata" class="nitrato-de-plata">
                                Nitrato de plata</a></li>

                            <li>
                                <a href="polvo-de-plata" title="Polvo de plata" class="polvo-de-plata">
                                Polvo de plata</a></li>

                            <li>
                                <a href="granallado-de-plata" title="Granallado de plata" class="granallado-de-plata">
                                Granallado de plata</a></li>

                            <li>
                                <a href="carbonato-de-plata" title="Carbonato de plata" class="carbonato-de-plata">
                                Carbonato de plata</a></li>

                            <li>
                                <a href="pasta-de-oro-brillante" title="Pasta de oro brillante" class="pasta-de-oro-brillante">
                                Pasta de oro brillante </a></li>

                            <li>            
                                <a href="pasta-de-platino-brillante" title="Pasta de platino brillante" class="pasta-de-platino-brillante">
                                Pasta de platino brillante</a></li>

                            <li>
                                <a href="oro-liquido-brillante" title="Oro l&iacute;quido brillante" class="oro-liquido-brillante">
                                Oro l&iacute;quido brillante</a></li>

                            <li>
                                <a href="pasta-de-oro" title="Pasta de oro" class="pasta-de-oro">
                                Pasta de oro</a></li>

                            <li>
                                <a href="platino-liquido-brillante" title="Platino l&iacute;quido brillante" class="platino-liquido-brillante">
                                Platino l&iacute;quido brillante</a></li>

                            <li>
                                <a href="pasta-de-platino-brillante-ceramica" title="Pasta de platino brillante cer&aacute;mica" class="pasta-de-platino-brillante-ceramica">
                                Pasta de platino brillante cer&aacute;mica</a></li>




                          </ul>
                      </div>

                </div>


                <div class="clear"></div>
         <div class="container-content">

            <div class="servicio-analisis">

            	<div class="gradient-top">
                	<h1>An&aacute;lisis qu&iacute;micos</h1>
                </div>

                <ul>
                    <li class="categorias">
                    An&aacute;lisis Qu&iacute;micos para la determinaci&oacute;n de contenidos de Oro (Au), Plata (Ag), Paladio (Pd) y Platino (Pt).</li>
                    <li class="categorias">
                    An&aacute;lisis qu&iacute;micos de tierras y minerales con contenidos de Oro (Au), Plata (Ag), platino (Pt) y paladio (Pt).</li>
                    <li class="categorias">An&aacute;lisis qu&iacute;micos de convertidores catal&iacute;ticos.</li>
                    <li class="categorias">Se certifica la pureza del oro, plata, platino y paladio.</li>
                </ul>
            </div>

            <div class="servicio-refinacion">
            	<div class="gradient-top">
                	<h1>Refinaci&oacute;n de metales</h1>
                </div>

                <ul>
                    <li class="categorias">Refinaci&oacute;n de Oro (Au), Plata (Ag), Paladio (Pd) y Platino (Pt).</li>
                    <li class="categorias">Refinaci&oacute;n escorias en peque&ntilde;o y gran volumen con contenidos de Oro, plata, platino y paladio.</li>
                    <li class="categorias">Refinaci&oacute;n de la tierra y basura con bajos contenidos de Oro (Au), plata (Ag), Platino (Pt) y Paladio (Pd).</li>
                    <li class="categorias">Refinaci&oacute;n de dores con oro (Au) y Plata (Ag) de la miner&iacute;a.</li>
                    <li class="categorias">Refinaci&oacute;n de precipitados de miner&iacute;a de oro (Au) y plata (Ag).</li>
                </ul>

            </div>

            <div class="servicio-recuperacion">
            	<div class="gradient-top">
                	<h1>Servicio de recuperaci&oacute;n</h1>
                </div>

            	<ul>
                    <li class="categorias">Recuperaci&oacute;n de metales preciosos de l&iacute;quidos y soluciones.</li>
                    <li class="categorias">Granallado y laminado de metales preciosos.</li>
                    <li class="categorias">Compra y venta de metales preciosos.</li>
                    <li class="categorias">Compra de catalizadores automotrices e industriales.</li>
                    <li class="categorias">Compramos chatarra con oro, plata, platino y paladio.</li>
                    <li class="categorias">Compramos escoria con contenidos en oro y plata.</li>
                    <li class="categorias">Compra de barros y tierras con contenidos de oro y plata.</li>
                    <li class="categorias">Compra de Scrap o Material Impregnado de Oro (Au) y Plata (Ag).</li>
                </ul>
            </div>

                <div class="clear"></div>
            <div class="acreditaciones-content-box">

                <div class="gradient-top">
                	<h1>ACREDITACIONES</h1>
                </div>

                <p>Se cuenta con acreditaci&oacute;n como Laboratorio de Ensayos ante la <strong>Entidad Mexicana de Acreditaci&oacute;n (ema)</strong> de acuerdo a los requisitos establecidos en la Norma Mexicana <strong>NMX-EC-17025-IMNC-2006</strong><br /><strong>(ISO/IEC 17025:2005)</strong> para las actividades de evaluaci&oacute;n de la conformidad en la rama <strong>Qu&iacute;mica.</strong></p>

        			<a href='popUps/certificado-de-acreditacion.html?TB_iframe=true&amp;height=499&amp;width=384&amp;scrolling=no;' title='Certificado de acreditaci&oacute;n' rel='sexylightbox'>
                    <img src="/img/imgs/nos2.png" class="imageAcreditacion" />
                    </a>

                <ul>
                    <li>Acreditaci&oacute;n No:  Q-0331-057/11</li>
                    <li>Vigente a partir del 2011-12-09</li>
                </ul>

            </div>



            <div class="certificaciones-content-box">
            	<div class="gradient-top">
                	<h1>CERTIFICACIONES</h1>
                </div>

                <p>La Planta de Refinaci&oacute;n se encuentra Certificada ante la <strong>Procuradur&iacute;a Federal de Protecci&oacute;n al Ambiente (PROFEPA) como Industria Limpia.</strong><br /><br /><br /><br /><br /></p>


                <a href='popUps/certificado-de-acreditacion-profepa.html?TB_iframe=true&amp;height=499&amp;width=384&amp;scrolling=no;' title='Certificado de acreditaci&oacute;n Profepa' rel='sexylightbox'>
                <img src="/img/imgs/nos3.png" class="imageCertificacion"/>
                </a>

            </div> <div style="clear:both;"></div>

		</div>


</div>
