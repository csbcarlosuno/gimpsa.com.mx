<div id="centralContainer" style="margin-top:-50px">	
	<div id="containerTraging">
	
		<h2>Comercializaci&oacute;n</h2>    
    
        <div class="container-comercio">
            <p>Somos especialistas en el ramo de la compra-venta de metales y minerales preciosos, complementando esta actividad con la afinaci&oacute;n y an&aacute;lisis de los mismos en nuestros laboratorios y en la planta de refinaci&oacute;n con el reciclaje de materiales que contienen metales preciosos.</p>
        
            <p>Producimos y comercializamos entre otros productos, oro y plata finos, en barra, l&aacute;mina y granalla, as&iacute; como nitrato de plata, cianuro de oro y cloruro de oro, adem&aacute;s platino y paladio.</p>
        
            <p>La comercializaci&oacute;n de nuestros productos la realizamos a trav&eacute;s de nuestras sucursales en el Distrito Federal y en las ciudades de Guadalajara, Jalisco y Taxco, Guerrero. Actualmente atendemos un importante porcentaje del mercado nacional.</p>
       </div>     
       
        <img src="/img/comercio-procesos.jpg" class="proceso-comercio" />
        <div class="clear" style="border-bottom: 1px dashed #CCC; padding-top:30px;"></div>

        <h2> Servicio que ofrecemos</h2>
        
        <p>Debido al apoyo de bancos y proveedores extranjeros como Bank Boston, Mocatta, Republic National Bank, Swiss Bank Corp. y Marc Precious Metals Inc. Ofrecemos otros servicios &uacute;nicos en el mercado como son: swaps, opciones, futuros, forwards y divisas. </p>
    
        <p>A continuaci&oacute;n se detalla en que consisten:</p>



        <div class="container-swap" class="image-swaps">    
            <h2> Swaps</h2>
            
            <p>Cuando los fabricantes e incluso distribuidores de joyer&iacute;a y orfebrer&iacute;a exportan sus productos de oro o plata, reciben, com&uacute;nmente, el pago de sus productos en metal f&iacute;sico. El swap permite al cliente depositar los f&iacute;sicos en un banco del extranjero, e Internacional de Metales se lo entrega en M&eacute;xico cobrando &uacute;nicamente una comisi&oacute;n por el servicio.</p>        
        </div>    
        
        <img src="/img/comercio-metales.jpg" class="image-swaps" />
        <div class="clear"></div>



        <div class="container-fowards">    
            <h2>Opciones, futuros y fowards</h2>
            
            <p>Estos son instrumentos de coberturas o ventas a futuro de metales preciosos, en los que de acuerdo a las necesidades del cliente, se pactan por anticipado precios a determinado lapso de tiempo. En este tipo de operaciones, Internacional de Metales Preciosos, financia la compra del futuro y el pago de la cobertura. Para ello se le requiere al cliente un dep&oacute;sito del 10% en garant&iacute;a de que la venta se cumplir&aacute; en las condiciones establecidas.</p>	
        </div>
        
        <img src="/img/comercio-metales-preciosos.jpg" class="image-precious-metals" />  
        <div class="clear" style="border-bottom: 1px dashed #CCC; padding-top:30px; margin-top:40px;"></div>
        
    
        <h2> Divisas</h2>
        
        <p>El precio de los metales preciosos para la compra y venta, se rige de acuerdo al mercado internacional sobre la base de d&oacute;lares americanos. Esto crea la necesidad de esta divisa en vol&uacute;menes considerables de acuerdo a las ventas diarias de metales, por lo cual tenemos una relaci&oacute;n estrecha y constante con Casas de Cambio y Bancos a los que compramos y vendemos d&oacute;lares en condiciones muy favorables, mismas que transmitimos a nuestros clientes.</p>
    
	</div>

        <?php include('contenedor_lateral_derecho_trading.php');?>
    	<div class="clear"></div>
    </div>   