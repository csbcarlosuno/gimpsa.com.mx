<link rel='stylesheet' type='text/css' id='camera-css' href='/css/camera.css' />
<link rel='stylesheet' type='text/css' href='/css/skin.css'>
<script type='text/javascript' src='/camera/scripts/jquery.min.js'></script>
<script type='text/javascript' src='/camera/scripts/jquery.mobile.customized.min.js'></script>
<script type='text/javascript' src='/camera/scripts/jquery.easing.1.3.js'></script> 
<script type='text/javascript' src='/camera/scripts/camera.min.js'></script> 

<div id="centralContainer">	


	<h1>SERVICIOS</h1> 
	<div id="contenedorProducto">
    	
        <div class="contenedorProducto_descripcion"> 
                  
                  		<img src="/img/imgServicios/analisis_metalico.jpg" title="Analisis met&aacute;lico" class="queryImg-productos" />
						
                        <h2 style="margin-top: 0;">An&aacute;lisis met&aacute;lico</h2>
                        
                        <h3>M&eacute;todo</h3>                  
                  
                        <ul class="services-list"> 
                            <li class="categorias">Copelaci&oacute;n</li>
                            <li class="categorias" style="width:100%">Volhard (S&oacute;lo para la determinaci&oacute;n de Ag)</li>
                            <li class="categorias">Pistola de Rayos X</li> 
                        </ul> 
                        
                        <div class="clear"></div>
                          
                     	<h3>Elementos a Analizar</h3> 
                        
                        <ul class="services-list">
                        	<li class="categorias">Oro (Au)</li>
                            <li class="categorias">Plata (Ag)</li>
                            <li class="categorias">Paladio (Pd)</li>
                            <li class="categorias">Platino (Pt)</li>
                        </ul>
                        
                        <div class="clear"></div>
                              
                      	<h3>Cantidad necesaria de muestra</h3> 

                       	<p>6g. La muestra debe de ser completamente homog&eacute;nea.</p>                                                    
                       
                       	<div class="clear"></div>
                        
                        <a href="contacto" title="Contacta con nosotros" class="contacta">Cont&aacute;ctanos</a>                		
                          
		</div>
                  
                  
                  		<script type="text/javascript">
							 jQuery(document).ready(function() {
							   jQuery("#slideCamera").camera({
								 height: '65%',
								 thumbnails: false
							   });
							 });
							 
					    </script>	                  		
                        
                        <div style="float:left; width:47%;">                        
                        	<div class="fluid_container">
                            <div class="camera_wrap camera_azure_skin" id="slideCamera">
                                <div data-thumb="/img/imgServicios/thumbs/analisis_metalico_thumbs.jpg" data-src="/img/imgServicios/analisis_metalico.jpg"></div>       
                                <div data-thumb="/img/imgServicios/thumbs/analisis_metalico_thumbs_2.jpg" data-src="/img/imgServicios/analisis_metalico_2.jpg"></div>
                            </div>
    						</div>
                        </div>
                        
                        
    					<div class="clear"></div>   

                        <div class="atencionCliente">
                            <p><strong>Atenci&oacute;n al cliente</strong></p>
                            <img src="/img/atencioCliente.jpg" width="47" height="47" />
                            <span>DF (52) 5121-1892</span>
                            <span>Lunes a viernes 7am a 4pm</span>
                       </div>
                                   
    	</div>  
            
   		<?php include('contenedor_lateral_derecho_servicios.php');?>
            
            
            
            
			<div class="clear"></div>		
</div>      