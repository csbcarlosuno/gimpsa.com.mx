	<div class='conteinerNews'>
            	
                <h3>SERVICIOS</h3>                
                <div class="accordion" id="accordion2">
                  
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <strong><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                        An&aacute;lisis
                      </a></strong>
                    </div>
                    
                            <div id="collapseOne" class="accordion-body collapse">
                              <div class="accordion-inner">
                                <a href="analisis-de-tierras">Tierras</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="analisis-metalico">Met&aacute;lico</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="analisis-de-joyeria">Joyer&iacute;a</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="analisis-de-escoria">Escoria</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="analisis-de-mineral">Mineral</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="analisis-de-soluciones">Soluciones</a>
                              </div>
                            </div>
                  </div>
                      
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <strong><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                        Refinaci&oacute;n
                      </a></strong>
                    </div>
                        
                            <div id="collapseTwo" class="accordion-body collapse">
                              <div class="accordion-inner">
                                <a href="refinacion-de-oro">Oro</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="refinacion-de-plata">Plata</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="refinacion-de-paladio">Paladio</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="refinacion-de-platino">Platino</a>
                              </div>
                            </div>                        
                  </div>                                    
                  
                  
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <strong><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree">
                        Fundici&oacute;n
                      </a></strong>
                    </div>
                            <div id="collapseThree" class="accordion-body collapse">
                              <div class="accordion-inner">
                                <a href="fundicion-metalicos">Met&aacute;licos</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="fundicion-no-metalicos">No met&aacute;licos</a>
                              </div>
                            </div>                        
                  </div>
                  
 			      <hr />	
                  <div class="laboratorio-tercero"><a href="laboratorio-tercero">Laboratorio tercero</a></div>
                  <hr />	
                  
                  
                  <h3>PRODUCTOS</h3>                
                
                  
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <strong><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapseFour">
                        Oro
                      </a></strong>
                    </div>
                            <div id="collapseFour" class="accordion-body collapse in">
                              <div class="accordion-inner">
                                <a href="resinato-de-oro">Resinato de oro</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="#">Cloruro de oro</a>
                              </div>
                            </div>
                 </div>                 
                      
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <strong><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion5" href="#collapseFive">
                        Plata
                      </a></strong>
                    </div>
                            <div id="collapseFive" class="accordion-body collapse">
                              <div class="accordion-inner">
                                <a href="cloruro-de-plata">Cloruro de plata</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="nitrato-de-plata">Nitrato de plata</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="polvo-de-plata">Polvo de plata</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="granallado-de-plata">Granalla de plata</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="carbonato-de-plata">Carbonato de plata</a>
                              </div>
                            </div>                        
                  </div>
                                    
                  <p class="secondary-content">PASTAS</p>
                                    
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <strong><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion6" href="#collapseSix">
                        Vidrio
                      </a></strong>
                    </div>
                            <div id="collapseSix" class="accordion-body collapse">
                              <div class="accordion-inner">
                                <a href="pasta-de-oro-brillante">Pasta de oro brillante</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="pasta-de-platino-brillante-para-vidrio">Pasta de platino brillante</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="oro-liquido-brillante">Oro l&iacute;quido brillante</a>
                              </div>
                            </div>                        
                  </div>
                  
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <strong><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion7" href="#collapseSeven">
                        Cer&aacute;mica
                      </a></strong>
                    </div>
                            <div id="collapseSeven" class="accordion-body collapse">
                              <div class="accordion-inner">
                                <a href="pasta-de-oro">Pasta de oro</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="pasta-de-platino-brillante-para-ceramica">Pasta de platino brillante</a>
                              </div>
                              <div class="accordion-inner">
                                <a href="platino-liquido-brillante">Platino l&iacute;quido brillante</a>
                              </div>
                            </div>                        
                  </div>
 			      <hr />
                                     
                </div>
 

            </div>
            