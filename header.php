<?php include('php/class.yahoostock.php'); ?>
<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />		 
	<link href='/img/iconG.png' rel='shortcut icon' type='image/x-icon'>
	<link rel='stylesheet/less' type='text/css' href='/css/estilos.less'/>
    <link rel='stylesheet' type='text/css' href='/css/estilos.css' />
    <link rel='stylesheet' type='text/css' href='/popUps/css/sexylightbox.css' media='all' />
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.css" />
	
    <script src='/js/less-1.3.3.min.js'></script>
	<script src='http://code.jquery.com/jquery-1.9.1.js'></script>
 	<script src='http://code.jquery.com/ui/1.10.3/jquery-ui.js'></script>
	<script src='/js/funciones.js'></script>
	<!-- PopUp Mootools -->
	<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/mootools/1.2.3/mootools-yui-compressed.js'></script>
	<script type='text/javascript' src='/popUps/js/sexylightbox.v2.3.mootools.min.js'></script>
    <script type='text/javascript' src='/popUps/js/sexylightbox.js'></script>
	<!--jCarousel--> 
    <script type='text/javascript' src='/js/jquery.jcarousel.min.js'></script>
    <script type='text/javascript' src='/js/bootstrap-dropdown.js'></script>
    <script type='text/javascript' src='/js/bootstrap-collapse.js'></script>
    <script type='text/javascript' src='/js/bootstrap-transition.js'></script>
    
</head>
<body>


	<div id='menuOculto'>
                        <ul>
                            <li><a href='http://www.gimpsaelectrical.com/' title='GIMPSA EL&Eacute;CTRICAL'>GIMPSA ELECTRICAL</a></li>
                            <li><a href='http://1csb.mx/' title='Call Center'>UNO</a></li>
                            <li><a href='http://www.prendal.com/' title='Casa de empe&ntilde;o'>PRENDAL</a></li>
                        </ul>
	</div>				
						
			<div class='clear'></div>
						
	<div id='backgroundTop'>
                        <div id='topContainer'>
                            <div id='logo'><a href='inicio' title='Gimpsa'>
                            <img src='/img/logo.png' /></a></div>
                            <span id='btnArrow'>
                                <a href='#' id='abrirMenuOculto' title='Corporativo GIMPSA'></a>
                            </span>
                            <!--
                            <marquee scrollamount=2 scrolldelay=10>
							<?php /*
								function priceMetals(){

									$objYahooStock = new YahooStock;
									$objYahooStock->addFormat("snl1d1t1cv");
						
                                    //Precios metales preciosos ORO; COBRE; PALADIO; PLATINO; PLATA
                                    $objYahooStock->addStock("GCU13.CMX");
                                    $objYahooStock->addStock("HGU13.CMX");
                                    $objYahooStock->addStock("PAU13.NYM");
                                    $objYahooStock->addStock("PLU13.NYM");
                                    $objYahooStock->addStock("SIU13.CMX");
                                    
                                    foreach( $objYahooStock->getQuotes() as $code => $stock)
                                    {
                                        ?>
                                        Precio actual: <?php echo '&nbsp;'.$stock[1].'&nbsp;'; ?> 
                                        
                                        $<?php echo $stock[2]; ?>&nbsp; &nbsp; | &nbsp; &nbsp;
                                        <?php
                                    }
                                    
                        	}
							
							 priceMetals(); */?>
                            
                            </marquee> -->					
                        </div>											 
	</div>																			
    					
			<div class='cromo'></div>

							
	<div id="centralContainer">						
                                    
                                <div class="navbar">
                                    <div class="navbar-inner">
                                      <div class="container">
                                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                          <span class="icon-bar"></span>
                                          <span class="icon-bar"></span>
                                          <span class="icon-bar"></span>
                                        </a>
                                        <a class="brand" href="#">MENÚ</a>
                                    <!--    <a class="brand" href="#">Project name</a>-->
                                                <div class="nav-collapse collapse">
                                                  <ul class="nav">
                                                    <li class="active"><a href="inicio">Inicio</a></li>
                                                    <li><a href="quienes-somos">Quienes Somos</a></li>
                                                    <li class="dropdown">
                                                    	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Productos<b class="caret"></b></a>
                                                    	  
                                                          <ul class="dropdown-menu">
                                                            <li class="dropdown-submenu"><a href="#">Oro</a>
                                                            	<ul class="dropdown-menu">
                                                                  <li><a tabindex="-1" href="resinato-de-oro">Resinato de Oro</a></li>
                                                                  <li><a tabindex="-1" href="#">Cloruro de Oro</a></li>
                                                                </ul>
                                                            </li>                                                            
                                                            <li class="dropdown-submenu"><a href="#">Plata</a>
                                                            	<ul class="dropdown-menu">
                                                                   <li><a tabindex="-1" href="cloruro-de-plata">Cloruro de Plata</a></li>
                                                                   <li><a tabindex="-1" href="nitrato-de-plata">Nitrato de Plata</a></li>
                                                                   <li><a tabindex="-1" href="polvo-de-plata">Polvo de Plata</a></li>
                                                                   <li><a tabindex="-1" href="granallado-de-plata">Granallado de Plata</a></li>
                                                                   <li><a tabindex="-1" href="carbonato-de-plata">Carbonato de Plata</a></li> 
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li class="nav-header">Pastas</li>
                                                            
                                                            <li class="dropdown-submenu"><a href="#">Vidrio</a>
                                                            	<ul class="dropdown-menu">
                                                                   <li><a tabindex="-1" href="pasta-de-oro-brillante">Pasta de oro brillante</a></li>
                                                                   <li><a tabindex="-1" href="pasta-de-platino-brillante-para-vidrio">Pasta de platino brillante</a></li>
                                                                   <li><a tabindex="-1" href="oro-liquido-brillante">Oro l&iacute;quido brillante</a></li>
                                                                </ul>
                                                            </li>    
															<li class="dropdown-submenu"><a href="#">Cer&aacute;mica</a>
                                                            	<ul class="dropdown-menu">
                                                                	<li><a tabindex="-1" href="pasta-de-oro">Pasta de Oro</a></li>
                                                                    <li><a tabindex="-1" href="pasta-de-platino-brillante-para-ceramica">Pasta de platino brillante</a></li>
                                                                    <li><a tabindex="-1" href="platino-liquido-brillante">Platino l&iacute;quido brillante</a></li>	
                                                                </ul>
                                                            </li>
                                                          </ul>
                                                    </li>                                                    
                                                    <li class="dropdown">
                                                    	  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Trading<b class="caret"></b></a>
                                                    	  <ul class="dropdown-menu">
                                                          	<li><a href="comercializacion-de-metales">Comercio</a></li>
                                                          	<li><a href="compra-de-metales">Compra de metales</a></li>
                                                    		<li><a href="venta-de-metales">Venta de metales</a></li>
                                                    		<li><a href="transporte-de-valores">Transporte de valores</a></li>
                                                          </ul> 
                                                    </li>             
                                                    <li class="dropdown">
                                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Servicios<b class="caret"></b></a>
                                                          
                                                          <ul class="dropdown-menu">
                                                            <li class="dropdown-submenu"><a href="#">An&aacute;lisis</a>
                                                            	<ul class="dropdown-menu">
                                                                	<li><a tabindex="-1" href="analisis-de-tierras">Tierras</a></li>
                                                                    <li><a tabindex="-1" href="analisis-metalico">Met&aacute;lico</a></li>
                                                                    <li><a tabindex="-1" href="analisis-de-joyeria">Joyer&iacute;a</a></li>
                                                                    <li><a tabindex="-1" href="analisis-de-escoria">Escoria</a></li>
                                                                    <li><a tabindex="-1" href="analisis-de-mineral">Mineral</a></li>
                                                                    <li><a tabindex="-1" href="analisis-de-soluciones">Soluciones</a></li>                                                                                                                                        
                                                                </ul>                                                                
                                                            </li>
                                                            
                                                            <li class="dropdown-submenu"><a href="#">Refinaci&oacute;n</a>
                                                            	<ul class="dropdown-menu">
                                                                	<li><a tabindex="-1" href="refinacion-de-oro">Oro</a></li>
                                                                    <li><a tabindex="-1" href="refinacion-de-plata">Plata</a></li>
                                                                    <li><a tabindex="-1" href="refinacion-de-paladio">Paladio</a></li>
                                                                    <li><a tabindex="-1" href="refinacion-de-platino">Platino</a></li>
                                                                </ul>
                                                            </li>   
                                                            
                                                            <li class="dropdown-submenu"><a href="#">Fundici&oacute;n</a>
                                                            	<ul class="dropdown-menu">
                                                                	<li><a tabindex="-1" href="fundicion-metalicos">Met&aacute;licos</a></li>
                                                                    <li><a tabindex="-1" href="fundicion-no-metalicos">No Met&aacute;licos</a></li>
                                                                </ul>
                                                            </li>
                                                            <li><a href="laboratorio-tercero">Laboratorio tercero</a></li>                                                       
                                                          </ul>
                                                    </li>
                                                    <li><a href="contacto">Contacto</a></li>
                                                    <li class="english"><a href="#">English Version</a></li>  
                                                    
                                                  </ul>
          
                                                </div>
                                      </div>
                                    </div>
                                  </div>	
    
    
    
    
    </div>