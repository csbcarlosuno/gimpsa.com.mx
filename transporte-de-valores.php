<div id="centralContainer" style="margin-top:-50px">	
	<div id="containerTraging">
    
    <h2>Transporte de valores</h2>
    
    <p>En Grupo IMPSA contamos con una flotilla de camionetas y camiones blindados a trav&eacute;s de los cuales ofrecemos el servicio de manejo, custodia y traslado de valores. El transporte de bienes, documentos o efectivo se controla a trav&eacute;s de un grupo especializado en el &aacute;rea de log&iacute;stica, ofreciendo un eficiente traslado de valores por diferentes rutas, controladas por nuestro equipo de seguridad para llegar puntualmente a la entrega de los servicios.</p>

	<p>Adem&aacute;s de contar con un equipo de guardias y custodios armados, la cobertura de los valores es respaldada por una amplia p&oacute;liza de seguro.</p>

	<p>En IMPSA traslado de valores, nos hemos especializado en el servicio de recolecci&oacute;n, traslado y entrega de valores en la Zona Metropolitana y otras ciudades importantes en el interior de la rep&uacute;blica, por lo que contamos con un servicio din&aacute;mico y ante todo seguro, con costos muy accesibles para las empresas.</p>

	<p style="margin-bottom:20px;">Como ejemplo IMPSA realiza entre sus servicios para tarjetas telef&oacute;nicas el proceso de recolecci&oacute;n, traslado y entrega de valores en Servicio For&aacute;neo de la siguiente forma:</p>


	<ul style="width:44%; margin-top:45px;">
		<li>Recolecci&oacute;n en las oficinas que el cliente indique</li>
    	<li>Traslado al centro de acopio de IMPSA para su separaci&oacute;n y empaque por monto, destino y cliente.</li>
		<li>Env&iacute;o a los clientes con un plazo de entrega de entre 24 y 48 hrs.</li>
		<li>Servicio de informaci&oacute;n y rastreo v&iacute;a Internet de los env&iacute;os</li>
		<li>Reporte quincenal de tarjetas trasladadas por destino, plaza, cliente y monto enviado.</li>
		<li>Costo competitivo como servicio for&aacute;neo.</li>
	</ul>
    
    <div class="container-transporte-de-valores">    
    	<img src="/img/transporte-esquema.jpg" />
    </div>
    
    <div class="clear"></div>
	<p style="margin-top:50px;"><strong>En IMPSA ponemos a sus &oacute;rdenes esta divisi&oacute;n de traslado de valores en la que encontrar&aacute; la seguridad y eficacia que su negocio necesita para incrementar su productividad.</strong></p>
    
    </div>
    <?php include('contenedor_lateral_derecho_trading.php');?>
</div>