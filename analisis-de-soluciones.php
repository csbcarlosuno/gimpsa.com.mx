<link rel='stylesheet' type='text/css' id='camera-css' href='/css/camera.css' />
<link rel='stylesheet' type='text/css' href='/css/skin.css'>
<script type='text/javascript' src='/camera/scripts/jquery.min.js'></script>
<script type='text/javascript' src='/camera/scripts/jquery.mobile.customized.min.js'></script>
<script type='text/javascript' src='/camera/scripts/jquery.easing.1.3.js'></script> 
<script type='text/javascript' src='/camera/scripts/camera.min.js'></script> 

<div id="centralContainer">	


	<h1>SERVICIOS</h1> 
	<div id="contenedorProducto">
    	
        <div class="contenedorProducto_descripcion"> 
                  
                  		<img src="/img/imgServicios/analisis_de_soluciones.jpg" title="An&aacute;lisis de soluciones" class="queryImg-productos" />
						
                        
                        <h2 style="margin-top: 0;">An&aacute;lisis de soluciones</h2>
                        
                        
                        <h3>M&eacute;todo</h3>                  
                  
                        <ul class="services-list"> 
                            <li class="categorias">Copelaci&oacute;n</li>  
                            <li class="categorias" style="width:100%">An&aacute;lisis de Oro en soluci&oacute;n de Cianuro</li>
                            <li class="categorias" style="width:100%">Volhard (S&oacute;lo para la determinaci&oacute;n de Ag)</li>
                        </ul> 
                        
                        <div class="clear"></div>
                          
                     	<h3>Elementos a Analizar</h3> 
                        
                        <ul class="services-list">
                        	<li class="categorias">Oro (Au)</li>
                            <li class="categorias">Plata (Ag)</li>
                            <li class="categorias">Paladio (Pd)</li>
                            <li class="categorias">Platino (Pt)</li>
                        </ul>
                        
                        <div class="clear"></div>
                              
                      	<h3>Cantidad necesaria de muestra</h3> 

                       	<p>La muestra debe de ser representativa del total del Lote y ser completamente homog&eacute;nea.</p>
                                               
                       	<div class="clear"></div>
                        
                        <a href="contacto" title="Contacta con nosotros" class="contacta">Cont&aacute;ctanos</a>                		
                          
		</div>                                                     			                  		
                        
                        <img src="/img/imgServicios/analisis_de_soluciones.jpg" title="An&aacute;lisis de soluciones" class="imgsProductos" />
                        
                        
    					<div class="clear"></div>   

                        <div class="atencionCliente">
                            <p><strong>Atenci&oacute;n al cliente</strong></p>
                            <img src="/img/atencioCliente.jpg" width="47" height="47" />
                            <span>DF (52) 5121-1892</span>
                            <span>Lunes a viernes 7am a 4pm</span>
                       </div>
                                   
    	</div>  
            
   		<?php include('contenedor_lateral_derecho_servicios.php');?>
            
            
            
            
			<div class="clear"></div>		
</div>      