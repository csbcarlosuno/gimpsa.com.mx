// JavaScript Document

	$(document).ready(function() {
    	$('#mycarousel').jcarousel({
		scroll:	1,
		visible: 6,
        wrap: 'last',
        initCallback: mycarousel_initCallback
    });
	
	/*Men&uacute; oculto*/
	     var menu2 = $( "#menuOculto" );
		 var datosForm = $( "#effect" );
		 
		 $( "#abrirMenuOculto" ).click(function() {
			 menu2.toggle('slow');
			 var arrow = $( this ).css( "background-position" );
		 });
	
	/*jQUERY PARA CAT&aacute;LOGO*/	
	
		 $( "#button" ).click(function() {
			  var valida = enviarDatos();
			  if(valida)
			  {
			 	datosForm.show( "blind", 500 );		
			  }
		 });
		
		 $( "#menuOculto" ).hide();
		 $( "#effect" ).hide();
	
});

	

/*Funci&oacute;n para Carousel*/
function mycarousel_initCallback(carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};

function mostrar(){ 
		document.getElementById('oculto').style.display = 'block';} 
		
		
/*VALIDACION FORMULARIO*/

function valEmail(valor){
	// Cortes&iacute;a de http://www.ejemplode.com
    re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
    if(!re.exec(valor)){
        return false;
    }else{
        return true;
    }
}


function enviarDatos(){
	var f=document.Contacto;
	
	if(f.nombre.value==0){
		alert("Ingresa tu nombre");
		f.nombre.value="";
		f.nombre.focus();
		return false;
	}
	
	if(f.email.value==0){
		alert("Ingresa un correo electr&oacute;nico");
		f.email.value="";
		f.email.focus();
		return false;
	}
	
	if(valEmail(f.email.value)==0){
		alert("Ingresa un correo electr&oacute;nico valido");
		f.email.value="";
		f.email.focus();
		return false;
	}	

		
	if(f.comentario.value ==0){
		alert("Ingresa tu mensaje");
		f.comentario.value ="";
		f.comentario.focus();
		return false;
	}

	f.submit();
	return true;
}
		
		
		
		/**Mps de google**/
		
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(19.425493,-99.175631),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP   
        };
		
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
		
		var marcador = new google.maps.Marker({
		  position: new google.maps.LatLng(19.425493,-99.175631),/*19.616354,-99.044173*/
		  map: map,
		  title: 'Ponte en contacto con nosotros, con gusto te atenderemos',
		  icon: 'img/marcaMaps.png'
		});
	}

		
		
		